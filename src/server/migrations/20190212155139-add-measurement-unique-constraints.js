"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint("Measurements", ["CountryId", "year"], {
      type: "unique",
      name: "country_year_unique",
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint("Measurements", "country_year_unique");
  }
};
