const axios = require("axios");
const fs = require("fs");
const path = require("path");
const unzipper = require("unzipper");
const csvParse = require("csv-parse");
const _ = require("lodash");

const db = require("../../models");


const DOWNLOAD_PATH = path.resolve(__dirname, "../../../../tmp");
const POPULATION_ZIP_PATH = path.resolve(DOWNLOAD_PATH, "population.zip");
const EMISSIONS_ZIP_PATH = path.resolve(DOWNLOAD_PATH, "emissions.zip");
const POPULATION_URL = "http://api.worldbank.org/v2/en/indicator/SP.POP.TOTL?downloadformat=csv";
const EMISSIONS_URL = "http://api.worldbank.org/v2/en/indicator/EN.ATM.CO2E.KT?downloadformat=csv";
const POPULATION_IDENTIFIER = "SP.POP.TOTL";
const EMISSIONS_IDENTIFIER = "EN.ATM.CO2E.KT";

// These country codes are used to filter out aggregated values in the World Bank data.
// Courtesy of Wikipedia: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
const acceptableCountries = [ "ABW", "AFG", "AGO", "AIA", "ALA", "ALB", "AND", "ARE", "ARG", "ARM", "ASM", "ATA", "ATF", "ATG", "AUS", "AUT", "AZE", "BDI", "BEL", "BEN", "BES", "BFA", "BGD", "BGR", "BHR", "BHS", "BIH", "BLM", "BLR", "BLZ", "BMU", "BOL", "BRA", "BRB", "BRN", "BTN", "BVT", "BWA", "CAF", "CAN", "CCK", "CHE", "CHL", "CHN", "CIV", "CMR", "COD", "COG", "COK", "COL", "COM", "CPV", "CRI", "CUB", "CUW", "CXR", "CYM", "CYP", "CZE", "DEU", "DJI", "DMA", "DNK", "DOM", "DZA", "ECU", "EGY", "ERI", "ESH", "ESP", "EST", "ETH", "FIN", "FJI", "FLK", "FRA", "FRO", "FSM", "GAB", "GBR", "GEO", "GGY", "GHA", "GIB", "GIN", "GLP", "GMB", "GNB", "GNQ", "GRC", "GRD", "GRL", "GTM", "GUF", "GUM", "GUY", "HKG", "HMD", "HND", "HRV", "HTI", "HUN", "IDN", "IMN", "IND", "IOT", "IRL", "IRN", "IRQ", "ISL", "ISR", "ITA", "JAM", "JEY", "JOR", "JPN", "KAZ", "KEN", "KGZ", "KHM", "KIR", "KNA", "KOR", "KWT", "LAO", "LBN", "LBR", "LBY", "LCA", "LIE", "LKA", "LSO", "LTU", "LUX", "LVA", "MAC", "MAF", "MAR", "MCO", "MDA", "MDG", "MDV", "MEX", "MHL", "MKD", "MLI", "MLT", "MMR", "MNE", "MNG", "MNP", "MOZ", "MRT", "MSR", "MTQ", "MUS", "MWI", "MYS", "MYT", "NAM", "NCL", "NER", "NFK", "NGA", "NIC", "NIU", "NLD", "NOR", "NPL", "NRU", "NZL", "OMN", "PAK", "PAN", "PCN", "PER", "PHL", "PLW", "PNG", "POL", "PRI", "PRK", "PRT", "PRY", "PSE", "PYF", "QAT", "REU", "ROU", "RUS", "RWA", "SAU", "SDN", "SEN", "SGP", "SGS", "SHN", "SJM", "SLB", "SLE", "SLV", "SMR", "SOM", "SPM", "SRB", "SSD", "STP", "SUR", "SVK", "SVN", "SWE", "SWZ", "SXM", "SYC", "SYR", "TCA", "TCD", "TGO", "THA", "TJK", "TKL", "TKM", "TLS", "TON", "TTO", "TUN", "TUR", "TUV", "TWN", "TZA", "UGA", "UKR", "UMI", "URY", "USA", "UZB", "VAT", "VCT", "VEN", "VGB", "VIR", "VNM", "VUT", "WLF", "WSM", "YEM", "ZAF", "ZMB", "ZWE" ];

/**
 * Download a zip file from a given URL.
 *
 * @param url
 * @param dest
 * @returns {Promise}
 */
const downloadZip = (url, dest) => new Promise((resolve, reject) => {
  const file = fs.createWriteStream(dest);

  axios.get(url, {
    responseType: "stream",
  }).then((response) => {
    response.data.pipe(file);
  }).catch(() => {
    reject("Downloading data failed");
  });

  file.on("finish", resolve);
  file.on("error", () => reject("Downloading data failed"));
});

/**
 * Unzip downloaded zip files. Extracts only the datafiles.
 *
 * @param zipFile
 * @param identifier
 * @returns {Promise}
 */
const unzip = (zipFile, identifier) => new Promise((resolve, reject) => {
  const file = fs.createReadStream(zipFile).pipe(unzipper.Parse());

  file.on("entry", entry => {
    if (!entry.path.includes("Metadata") && entry.path.includes(identifier)) {
      const target = fs.createWriteStream(path.resolve(DOWNLOAD_PATH, identifier + ".csv"));
      entry.pipe(target);

      target.on("error", () => reject("Unzipping files failed"));
    } else {
      entry.autodrain();
    }
  });

  file.on("close", resolve);
  file.on("error", () => reject("Unzipping files failed"));
});

/**
 * Parse population and emissions data from CSV files.
 *
 * @param identifier
 * @returns {Promise}
 */
const parseCsv = (identifier) => new Promise((resolve, reject) => {
  fs.readFile(path.resolve(DOWNLOAD_PATH, identifier + ".csv"), (error, data) => {
    if (error) return reject(error);

    csvParse(data, {
      skip_lines_with_error: true,
      relax_column_count: true,
      delimiter: ",",
      skip_empty_lines: true,
    }, (error, output) => {
      if (error) return reject("Parsing data failed");

      // Separate year numbers
      let years = [];
      for (let i = 4; i < output[1].length - 1; i++) {
        years.push(parseInt(output[1][i]) || null);
      }

      // Group data in year-value pairs
      // countryCode: {
      //   name: "...",
      //   values: {
      //      year: value,
      //      year: value,
      //   },
      // }
      let countries = {};
      for (let i = 2; i < output.length; i++) {
        let country = output[i];
        let values = country.slice(4, 4 + years.length).map(entry => parseFloat(entry) || null);

        let result = {};
        for (let i = 0; i < values.length; i++) {
          result[years[i]] = values[i];
        }

        countries[country[1]] = {
          name: country[0],
          values: result,
        };
      }

      resolve(countries);
    });
  });
});

/**
 * Combine population and emissions data into one object containing the data as key-value pairs.
 *
 * @param populationData
 * @param emissionsData
 */
const combineData = (populationData, emissionsData) => {
  let combinedData = {};

  for (let [code, countryData] of Object.entries(populationData)) {
    let data = combinedData[code] = {
      name: countryData.name,
      values: {},
    };

    for (let [year, population] of Object.entries(countryData.values)) {
      data.values[year] = { population };
    }
  }

  for (let [code, countryData] of Object.entries(emissionsData)) {
    for (let [year, emissions] of Object.entries(countryData.values)) {
      combinedData[code].values[year].emissions = emissions;
    }
  }

  return combinedData;
};

/**
 * Delete downloaded and unzipped files.
 */
const cleanTempFiles = () => {
  fs.readdir(DOWNLOAD_PATH, (error, files) => {
    for (const file of files) {
      fs.unlink(path.join(DOWNLOAD_PATH, file), () => {});
    }
  });
};

/**
 * Insert downloaded data into database.
 *
 * @param data
 */
const insertIntoDatabase = (data) => {
  let countries = [];
  let measurements = {};

  // Make data format Sequelize-friendly
  for (let [code, meta] of Object.entries(data)) {
    countries.push({
      code,
      name: meta.name,
    });

    measurements[code] = [];
    for (let [year, yearlyMeasurements] of Object.entries(meta.values)) {
      measurements[code].push({
        year: parseInt(year) || null,
        population: yearlyMeasurements.population || null,
        co2emissions: yearlyMeasurements.emissions || null,
      });
    }
  }

  // Create country entries
  return db.Country.bulkCreate(countries, {
    ignoreDuplicates: true,
  }).then(() => {
    // Find ids for the inserted countries
    return db.Country.findAll({
      attributes: [ "id", "code" ],
      where: {
        code: {
          [db.Sequelize.Op.in]: countries.map(country => country.code),
        },
      },
    });
  }).then(countries => {
    // Associate country id numbers with each corresponding measurement
    for (const country of countries) {
      for (let measurement of measurements[country.code]) {
        measurement.CountryId = country.id;
      }
    }

    // Insert measurements into database
    return db.Measurement.bulkCreate(Object.values(measurements).flat(1), {
      ignoreDuplicates: true,
    });
  }).catch(() => {
    return new Promise((resolve, reject) => reject("Updating database failed"));
  });
};

module.exports = (req, res) => {
  // Download zip files, unzip, parse and insert into database.
  Promise.all([
    downloadZip(POPULATION_URL, POPULATION_ZIP_PATH),
    downloadZip(EMISSIONS_URL, EMISSIONS_ZIP_PATH),
  ]).then(() => {
    return Promise.all([
      unzip(POPULATION_ZIP_PATH, POPULATION_IDENTIFIER),
      unzip(EMISSIONS_ZIP_PATH, EMISSIONS_IDENTIFIER),
    ]);
  }).then(() => {
    return Promise.all([
      parseCsv(POPULATION_IDENTIFIER),
      parseCsv(EMISSIONS_IDENTIFIER),
    ]);
  }).then(([populationData, emissionsData]) => {
    const combinedData = combineData(_.pick(populationData, acceptableCountries), _.pick(emissionsData, acceptableCountries));

    return insertIntoDatabase(combinedData);
  }).then(() => {
    res.sendStatus(201);
  }).catch((error) => {
    res.status(500).send(error);
  }).finally(() => {
    cleanTempFiles();
  });
};
