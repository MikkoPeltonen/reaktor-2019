const db = require("../../models");


module.exports = (req, res) => {
  db.Country.findAll({
    attributes: [ "code", "name" ],
  }).then(countries => {
    res.json(countries);
  });
};
