const express = require("express");

const getCountries = require("./getCountries");
const getMeasurements = require("./getMeasurements");
const updateDatabase = require("./updateDatabase");


const router = express.Router();

router.get("/countries", getCountries);
router.get("/measurements", getMeasurements);

router.post("/updateDatabase", updateDatabase);

module.exports = router;
