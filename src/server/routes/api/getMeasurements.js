const db = require("../../models");


module.exports = (req, res) => {
  const where = {};

  // Filter by country code
  if (req.query.includeCountries) {
    if (!where.hasOwnProperty("code")) where.code = {};
    where.code[db.Sequelize.Op.in] = req.query.includeCountries.split(",");
  }

  if (req.query.excludeCountries) {
    if (!where.hasOwnProperty("code")) where.code = {};
    where.code[db.Sequelize.Op.notIn] = req.query.excludeCountries.split(",");
  }

  let measurementsWhere = {};

  // Filter by year
  if (req.query.yearFrom) {
    measurementsWhere.year = {};
    measurementsWhere.year[db.Sequelize.Op.gte] = req.query.yearFrom;
  }

  if (req.query.yearTo) {
    if (!measurementsWhere.hasOwnProperty("year")) measurementsWhere.year = {};
    measurementsWhere.year[db.Sequelize.Op.lte] = req.query.yearTo;
  }

  // Filter by population
  if (req.query.populationFrom) {
    measurementsWhere.population = {};
    measurementsWhere.population[db.Sequelize.Op.gte] = req.query.populationFrom;
  }

  if (req.query.populationTo) {
    if (!measurementsWhere.hasOwnProperty("population")) measurementsWhere.population = {};
    measurementsWhere.population[db.Sequelize.Op.lte] = req.query.populationTo;
  }

  // Filter by emissions
  if (req.query.emissionsFrom) {
    measurementsWhere.co2emissions = {};
    measurementsWhere.co2emissions[db.Sequelize.Op.gte] = req.query.emissionsFrom;
  }

  if (req.query.emissionsTo) {
    if (!measurementsWhere.hasOwnProperty("co2emissions")) measurementsWhere.co2emissions = {};
    measurementsWhere.co2emissions[db.Sequelize.Op.lte] = req.query.emissionsTo;
  }

  db.Country.findAll({
    attributes: [ "code", "name" ],
    include: [{
      model: db.Measurement,
      attributes: [ "year", "population", "co2emissions" ],
      where: measurementsWhere,
    }],
    where,
  }).then(data => {
    if (!req.query.hasOwnProperty("normalize")) {
      return res.json(data);
    }

    // Normalize emissions per capita
    const results = [];
    for (const country of data) {
      const resultCountry = {
        code: country.code,
        name: country.name,
        Measurements: [],
      };

      for (const measurement of country.Measurements) {
        const population = measurement.population;
        let co2emissions;

        if (population) {
          // Normalized emissions in tons
          co2emissions = measurement.co2emissions * 1000 / population;
        } else {
          co2emissions = 0;
        }

        resultCountry.Measurements.push({
          population,
          co2emissions,
          year: measurement.year,
        });
      }

      results.push(resultCountry);
    }

    res.json(results);
  });
};
