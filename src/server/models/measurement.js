module.exports = (sequelize, DataTypes) => {
  const Measurement = sequelize.define("Measurement", {
    year: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    population: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    co2emissions: {
      type: DataTypes.DECIMAL,
      allowNull: true,
    },
  });

  Measurement.associate = (models) => {
    Measurement.belongsTo(models.Country);
  };

  return Measurement;
};
