module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define("Country", {
    code: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    }
  });

  Country.associate = (models) => {
    Country.hasMany(models.Measurement);
  };

  return Country;
};
