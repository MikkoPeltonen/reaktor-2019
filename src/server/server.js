const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const fs = require("fs");

if (process.env.NODE_ENV === "development") {
  require("dotenv").config({
    path: path.resolve(__dirname, "../../.env"),
  });
}


const DOWNLOAD_PATH = path.resolve(__dirname, "../../tmp");

const routes = require("./routes");

// Create temp folder for downloaded data
if (!fs.existsSync(DOWNLOAD_PATH)) fs.mkdirSync(DOWNLOAD_PATH);

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(path.resolve(__dirname, "../../dist")));

app.use("/", routes);

app.listen(process.env.PORT || 8080, () => {});
