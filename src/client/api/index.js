import { API_URL } from "../constants";

import getCountries from "./endpoints/getCountries";
import getMeasurements from "./endpoints/getMeasurements";
import updateDatabase from "./endpoints/updateDatabase";


class API {
  constructor() {
    this.url = API_URL;

    this.getAllCountries = getCountries.bind(this);
    this.getMesurements = getMeasurements.bind(this);
    this.updateDatabase = updateDatabase.bind(this);
  }
}

export default API;
