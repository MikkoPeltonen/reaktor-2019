import axios from "axios";


/**
 * Get a list of country names and codes from the API.
 *
 * @returns {Promise<AxiosResponse<any> | never>}
 */
function getCountries() {
  return axios.get(this.url + "/countries").then(result => new Promise((resolve, reject) => resolve(result.data)));
}

export default getCountries;
