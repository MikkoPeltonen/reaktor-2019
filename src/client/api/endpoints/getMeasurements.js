import axios from "axios";


/**
 * Get all, unfiltered results from the API.
 *
 * The data set is small, so it is faster and easier to do the filtering on the client side.
 *
 * @returns {Promise<AxiosResponse<any> | never>}
 */
function getMeasurements() {
  return axios.get(this.url + "/measurements").then(result => new Promise((resolve, reject) => resolve(result.data)));
}

export default getMeasurements;
