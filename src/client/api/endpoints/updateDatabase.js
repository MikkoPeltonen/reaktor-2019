import axios from "axios";


/**
 * Request the server to update the database.
 *
 * @returns {AxiosPromise<any>}
 */
function updateDatabase() {
  return axios.post(this.url + "/updateDatabase");
}

export default updateDatabase;
