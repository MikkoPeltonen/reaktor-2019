import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import RangeFilter from "./filters/RangeFilter";
import CountryFilter from "./filters/CountryFilter";
import SelectedCountries from "./filters/SelectedCountries";

import CountryGrowthCurve from "./charts/CountryGrowthCurve";


class EmissionsByYear extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstRender: true,
      filters: {
        selectedCountries: [ "FIN", "SWE", "DNK", "NOR", "ISL" ],
        normalize: true,
        yearFrom: 2000,
        yearTo: 2010,
      },
      filteredData: [],
    };

    this.yearRangeFilterOptions = {
      fromLabel: "from year",
      toLabel: "to year",
    };

    this.updateFilters = this.updateFilters.bind(this);
    this.updateGraphs = this.updateGraphs.bind(this);
    this.getCountryGrowthCurveData = this.getCountryGrowthCurveData.bind(this);
    this.getSelectedCountriesWithNames = this.getSelectedCountriesWithNames.bind(this);
    this.addSelectedCountry = this.addSelectedCountry.bind(this);
    this.deleteSelectedCountry = this.deleteSelectedCountry.bind(this);
    this.resetSelectedCountries = this.resetSelectedCountries.bind(this);

    this.handleNormalizeChange = this.handleNormalizeChange.bind(this);
    this.handleYearRangeChange = this.handleYearRangeChange.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // Render automatically when app is launched
    if (this.state.firstRender && !!this.props.results.length) {
      this.setState({
        firstRender: false,
      });

      this.updateGraphs();
    }
  }

  /**
   * Apply new filters.
   *
   * @param newFilters
   * @param callback
   */
  updateFilters(newFilters, callback) {
    this.setState({
      filters: Object.assign({}, this.state.filters, newFilters),
    }, callback);
  }

  /**
   * Filter data based on current filters. Prevents graph rerenders on each change.
   */
  updateGraphs() {
    this.setState({
      filteredData: this.getCountryGrowthCurveData(),
    });
  }

  /**
   * Return an array of filtered countries containing measurements within the given year range.
   *
   * @returns {Array}
   */
  getCountryGrowthCurveData() {
    // Filter by country code
    let filteredResults;
    if (this.state.filters.selectedCountries.length !== 0) {
      filteredResults = Object.values(_.pickBy(this.props.results, country => {
        return this.state.filters.selectedCountries.includes(country.code);
      }));
    } else {
      filteredResults = this.state.filters.selectedCountries;
    }

    const data = [];
    for (const country of filteredResults) {
      // Get all measurements for the country in the given year range
      const measurements = Object.values(_.pickBy(country.Measurements, measurement => {
        return this.state.filters.yearFrom <= measurement.year && measurement.year <= this.state.filters.yearTo;
      }));


      const dataPoints = [];
      for (let i = 0; i < measurements.length; i++) {
        const population = measurements[i].population || 0;
        let co2emissions = measurements[i].co2emissions || 0;

        // Normalize, if needed
        if (this.state.filters.normalize) {
          if (population > 0) {
            co2emissions = co2emissions * 1000 / population;
          } else {
            co2emissions = 0;
          }
        }

        dataPoints.push({
          population,
          co2emissions,
          year: measurements[i].year,
        });
      }

      data.push({
        code: country.code,
        name: country.name,
        data: dataPoints,
      });
    }

    return data;
  }

  /**
   * Get a list of currently selected countries as code-name-objects.
   *
   * @returns {{code: T, name: null}[]}
   */
  getSelectedCountriesWithNames() {
    return this.state.filters.selectedCountries.map(code => {
      const country = this.props.countries.find(country => country.code === code);

      return {
        code,
        name: country ? country.name : null,
      };
    });
  }

  /**
   * Event handler for adding a new country to the list of currently selected countries.
   *
   * @param countryCode
   */
  addSelectedCountry(countryCode) {
    this.updateFilters({
      selectedCountries: [ ...this.state.filters.selectedCountries, countryCode ],
    });
  }

  /**
   * Remove a given country from the list of currently selected countries.
   *
   * @param countryCode
   */
  deleteSelectedCountry(countryCode) {
    this.updateFilters({
      selectedCountries: this.state.filters.selectedCountries.filter(code => code !== countryCode),
    });
  }

  resetSelectedCountries() {
    this.updateFilters({
      selectedCountries: [],
    });
  }

  /**
   * Handle normalize checkbox toggle event.
   *
   * @param event
   */
  handleNormalizeChange(event) {
    this.updateFilters({
      normalize: event.target.checked,
    }, () => this.updateGraphs());
  }

  /**
   * Handle year range input change event.
   *
   * @param yearRange
   */
  handleYearRangeChange(yearRange) {
    this.updateFilters({
      yearFrom: yearRange.from,
      yearTo: yearRange.to,
    });
  }

  render() {
    return (
      <>
        <div className="row">
          <div className="col-12">
            <h2>Population and CO<sub>2</sub> emissions by year</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-lg-9">
            <div className="row">
              <div className="col-12 col-lg-8">
                <RangeFilter
                  options={this.yearRangeFilterOptions}
                  from={this.state.filters.yearFrom}
                  to={this.state.filters.yearTo}
                  onChange={this.handleYearRangeChange} />
              </div>
              <div className="col-12 col-lg-4">
                <CountryFilter
                  countries={this.props.countries}
                  selectedCountries={this.state.filters.selectedCountries}
                  onCountrySelected={this.addSelectedCountry} />
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="form-group form-check">
                  <input
                    type="checkbox" id="normalize-emissions" className="form-check-input"
                    checked={this.state.filters.normalize}
                    onChange={this.handleNormalizeChange} />
                  <label htmlFor="normalize-emissions" className="form-check-label">Normalize emissions per capita</label>
                </div>
              </div>
            </div>
            <div className="row" style={{ marginBottom: 40 }}>
              <div className="col-12">
                <SelectedCountries
                  countries={this.getSelectedCountriesWithNames()}
                  deleteCountry={this.deleteSelectedCountry} />
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <div className="row">
              <div className="col-12 order-2 order-lg-1">
                <div className="form-group">
                  <label className="d-none d-lg-block">&nbsp;</label>
                  <button className="btn btn-primary form-control" onClick={this.updateGraphs}>do filtering</button>
                </div>
              </div>
              <div className="col-12 order-1 order-lg-2">
                <div className="form-group">
                  <label className="d-none d-lg-block">&nbsp;</label>
                  <button className="btn btn-light form-control" onClick={this.resetSelectedCountries}>clear countries</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <CountryGrowthCurve data={this.state.filteredData} isNormalized={this.state.filters.normalize} />
          </div>
        </div>
      </>
    );
  }
}

EmissionsByYear.propTypes = {
  results: PropTypes.array.isRequired,
  countries: PropTypes.array.isRequired,
};

export default EmissionsByYear;
