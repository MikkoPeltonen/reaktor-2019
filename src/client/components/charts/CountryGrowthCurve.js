import React from "react";
import PropTypes from "prop-types";
import Highcharts from "highcharts";


const colors = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"];
let unit = "kt";

const options = {
  title: {
    text: "",
  },
  xAxis: [{
    categories: [],
  }],
  yAxis: [{
    labels: {
      format: "",
    },
    title: {
      text: "CO2 emissions",
    },
  }, {
    title: {
      text: "Population",
    },
    opposite: true,
  }],
  plotOptions: {
    column: {
      events: {
        legendItemClick: () => false,
      },
    },
    allowPointSelect: false,
  },
  tooltip: {
    shared: true,
    useHTML: true,
    formatter() {
      // With two different data types plotted per country, we need to manually combine the series to display the
      // tooltip. linkedTo property is used to identify CO2 emissions data.
      const countries = [];
      for (const point of this.points) {
        const name = point.point.series.name;

        if (countries[name] === undefined) countries[name] = {};

        if (point.series.userOptions.linkedTo) {
          countries[name].co2emissons = point.y;
        } else {
          countries[name].population = point.y;
        }
      }

      const rows = Object.entries(countries).map(([name, data]) => {
        return `
          <tr>
            <td>${name}</td>
            <td>${data.population}</td>
            <td>${data.co2emissons.toFixed(2)} ${unit}</td>
          </tr>
        `;
      }).join("");

      return `
        <strong>${this.x}</strong>
        <table class="table-sm">
          <thead>
            <tr>
              <th>Country</th>
              <th>Population</th>
              <th>CO2 emissions</th>
            </tr>
          </thead>
          <tbody>
            ${rows}
          </tbody>
        </table>
      `;
    },
  },
  series: [],
};

class CountryGrowthCurve extends React.PureComponent {
  constructor(props) {
    super(props);

    this.container = React.createRef();

    this.updateChart = this.updateChart.bind(this);
  }

  componentDidMount() {
    this.chart = Highcharts.chart(this.container.current, options, undefined);
  }

  componentDidUpdate() {
    this.updateChart();
  }

  componentWillMount() {
    if (this.chart) {
      this.chart.destroy();
      this.chart = null;
    }
  }

  updateChart() {
    if (this.props.data.length === 0) {
      options.xAxis[0].categories = [];
      options.series = [];

      this.chart.update(options, true, true, true);

      return;
    }

    // Reformat data as series for Highcharts
    const series = [];
    let colorIndex = 0;
    for (const country of this.props.data) {
      if (colorIndex === colors.length) colorIndex = 0;

      series.push({
        name: country.name,
        type: "column",
        yAxis: 1,
        zIndex: 0,
        color: colors[colorIndex],
        data: country.data.map(measurement => measurement.population),
      }, {
        name: country.name,
        type: "spline",
        linkedTo: ":previous",
        zIndex: 1,
        color: colors[colorIndex],
        data: country.data.map(measurement => measurement.co2emissions),
      });

      colorIndex++;
    }

    // Get year range from data
    const sampleCountry = this.props.data[0];
    const fromYear = sampleCountry.data[0].year;
    const toYear = sampleCountry.data[sampleCountry.data.length - 1].year;

    // Create an array of year numbers to use as labels
    options.xAxis[0].categories = Array(toYear - fromYear + 1).fill().map((_, i) => fromYear + i);
    options.series = series;

    if (this.props.isNormalized) {
      options.yAxis[0].labels.format = "{value} t";
      unit = "t";
    } else {
      options.yAxis[0].labels.format = "{value} kt";
      unit = "kt";
    }

    // Redraw, force series and axis update. A complete rerender is required because of changes in axis and series.
    this.chart.update(options, true, true, true);
  }

  render() {
    return React.createElement("div", { ref: this.container });
  }
}

CountryGrowthCurve.propTypes = {
  data: PropTypes.array.isRequired,
  isNormalized: PropTypes.bool.isRequired,
};

export default CountryGrowthCurve;
