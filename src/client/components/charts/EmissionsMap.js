import React from "react";
import PropTypes from "prop-types";
import Highcharts from "highcharts/highmaps";
import worldMap from "./highcharts/world";


const options = {
  title: {
    text: "",
  },
  mapNavigation: {
    enabled: true,
    buttonOptions: {
      align: "right",
      verticalAlign: "top",
    },
  },
  legend: {
    layout: "vertical",
    align: "left",
    verticalAlign: "middle",
  },
  colorAxis: {
    min: 0,
    type: "linear",
    labels: {
      format: "{value} kt",
    },
  },
  tooltip: {
    formatter() {
      return "";
    },
  },
  series: [
    {
      name: "CO2 Emissions",
      data: [],
      mapData: worldMap,
      joinBy: ["iso-a3", "code"],
    },
  ],
};

class EmissionsMap extends React.PureComponent {
  constructor(props) {
    super(props);

    this.container = React.createRef();

    this.updateChart = this.updateChart.bind(this);
  }

  componentDidMount() {
    this.chart = Highcharts.Map(this.container.current, options, undefined);
  }

  componentDidUpdate() {
    this.updateChart();
  }

  componentWillMount() {
    if (this.chart) {
      this.chart.destroy();
      this.chart = null;
    }
  }

  updateChart() {
    // Format data to be compatible with Highcharts
    const data = options.series[0].data = [];
    for (const country of this.props.data) {
      data.push({
        code: country.code,
        name: country.name,
        population: country.population,
        value: country.co2emissions,
      });
    }

    if (this.props.isNormalized) {
      options.colorAxis.labels.format = "{value} t";
      options.tooltip.formatter = function() {
        return `
          ${this.point.name}: <strong>${this.point.value.toFixed(2)} tons</strong>
          (place #${this.point.index + 1})
        `;
      }
    } else {
      options.colorAxis.labels.format = "{value} kt";
      options.tooltip.formatter = function() {
        return `
          ${this.point.name}: <strong>${this.point.value.toFixed(2)} kilotons</strong>
          (place #${this.point.index + 1})
        `;
      }
    }

    this.chart.update(options);
  }

  render() {
    return React.createElement("div", { ref: this.container });
  }
}

EmissionsMap.propTypes = {
  data: PropTypes.array.isRequired,
  isNormalized: PropTypes.bool.isRequired,
};

export default EmissionsMap;
