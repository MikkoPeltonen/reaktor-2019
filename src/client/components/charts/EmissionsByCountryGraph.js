import React from "react";
import PropTypes from "prop-types";
import Highcharts from "highcharts";


const options = {
  title: {
    text: "",
  },
  legend: false,
  xAxis: [{
    categories: [],
  }],
  yAxis: [{
    labels: {
      format: "",
    },
    title: {
      text: "CO2 emissions",
    },
  }],
  tooltip: {
    formatter() {
      return "";
    },
  },
  series: [
    {
      type: "column",
      data: [],
    },
  ],
};

class EmissionsByCountryGraph extends React.PureComponent {
  constructor(props) {
    super(props);

    this.container = React.createRef();

    this.updateChart = this.updateChart.bind(this);
  }

  componentDidMount() {
    this.chart = Highcharts.chart(this.container.current, options, undefined);
  }

  componentDidUpdate() {
    this.updateChart();
  }

  componentWillMount() {
    if (this.chart) {
      this.chart.destroy();
      this.chart = null;
    }
  }

  updateChart() {
    options.xAxis[0].categories = this.props.data.map(measurement => measurement.name);
    options.series[0].data = this.props.data.map(measurement => measurement.co2emissions);

    if (this.props.isNormalized) {
      options.yAxis[0].labels.format = "{value} t";
      options.tooltip.formatter = function() {
        return `
          ${this.x}: <strong>${this.y.toFixed(2)} tons</strong>
          (place #${this.point.index + 1})
        `;
      }
    } else {
      options.yAxis[0].labels.format = "{value} kt";
      options.tooltip.formatter = function() {
        return `
          ${this.x}: <strong>${this.y.toFixed(2)} kilotons</strong>
          (place #${this.point.index + 1})
        `;
      }
    }

    this.chart.update(options);
  }

  render() {
    return React.createElement("div", { ref: this.container });
  }
}

EmissionsByCountryGraph.propTypes = {
  data: PropTypes.array.isRequired,
  isNormalized: PropTypes.bool.isRequired,
};

export default EmissionsByCountryGraph;
