import React from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import _ from "lodash";

import RangeFilter from "./filters/RangeFilter";
import CountryFilter from "./filters/CountryFilter";
import SelectedCountries from "./filters/SelectedCountries";

import EmissionsMap from "./charts/EmissionsMap";
import EmissionsByCountryGraph from "./charts/EmissionsByCountryGraph";


class EmissionsByCountry extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstRender: true,
      filters: {
        year: 2010,
        selectedCountries: [ "USA", "CHN" ],
        countryFilteringMode: 3,
        normalize: true,
        populationFrom: 50000000,
        populationTo: 0,
      },
      filteredData: [],
    };

    this.populationRangeFilterOptions = {
      fromLabel: "population from",
      toLabel: "population to",
    };

    this.countryFilteringOptions = [
      { value: 1, label: "display all" },
      { value: 2, label: "include selected" },
      { value: 3, label: "exclude selected" },
    ];

    this.getCountryFilterMode = this.getCountryFilterMode.bind(this);
    this.getEmissionsByCountryData = this.getEmissionsByCountryData.bind(this);
    this.getSelectedCountriesWithNames = this.getSelectedCountriesWithNames.bind(this);
    this.updateFilters = this.updateFilters.bind(this);
    this.updateGraphs = this.updateGraphs.bind(this);
    this.resetSelectedCountries = this.resetSelectedCountries.bind(this);
    this.addSelectedCountry = this.addSelectedCountry.bind(this);
    this.deleteSelectedCountry = this.deleteSelectedCountry.bind(this);

    this.handleYearChange = this.handleYearChange.bind(this);
    this.handlePopulationRangeChange = this.handlePopulationRangeChange.bind(this);
    this.handleCountryFilteringModeChange = this.handleCountryFilteringModeChange.bind(this);
    this.handleNormalizeChange = this.handleNormalizeChange.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // Render automatically when app is launched
    if (this.state.firstRender && !!this.props.results.length) {
      this.setState({
        firstRender: false,
      });

      this.updateGraphs();
    }
  }

  /**
   * Apply new filters.
   *
   * @param newFilters
   * @param callback
   */
  updateFilters(newFilters, callback) {
    this.setState({
      filters: Object.assign({}, this.state.filters, newFilters),
    }, callback);
  }

  /**
   * Filter data based on current filters. Prevents graph rerenders on each change.
   */
  updateGraphs() {
    this.setState({
      filteredData: this.getEmissionsByCountryData(),
    });
  }

  /**
   * Return an array of all the countries containing the measurements of the given year.
   *
   * @returns Array of data
   */
  getEmissionsByCountryData() {
    const data = [];
    const hasSelectedCountries = this.state.filters.selectedCountries.length > 0;
    for (const country of this.props.results) {
      // Filter by county name
      if (this.state.filters.countryFilteringMode !== 1) {
        const countrySelected = this.state.filters.selectedCountries.includes(country.code);
        if (this.state.filters.countryFilteringMode === 3) {
          if (hasSelectedCountries && countrySelected) continue;
        } else {
          if (!countrySelected) continue;
        }
      }

      // Limit measurements to one year
      const measurement = _.find(country.Measurements, measurement => measurement.year === this.state.filters.year) || [];
      const population = measurement.population || 0;
      let co2emissions = measurement.co2emissions || 0;

      // Filter by population
      if (this.state.filters.populationTo === 0) {
        if (this.state.filters.populationFrom > 0 && population < this.state.filters.populationFrom) {
          continue;
        }
      } else {
        if (population < this.state.filters.populationFrom || population > this.state.filters.populationTo) {
          continue;
        }
      }

      if (this.state.filters.normalize) {
        if (population > 0) {
          // Emissions in tons if normalized
          co2emissions /= population / 1000;
        } else {
          co2emissions = 0;
        }
      }

      data.push({
        co2emissions,
        code: country.code,
        name: country.name,
      });
    }

    // Sort from biggest emissions to smallest
    return data.sort((a, b) => b.co2emissions - a.co2emissions);
  }

  /**
   * Get a list of currently selected countries as code-name-objects.
   *
   * @returns {{code: T, name: null}[]}
   */
  getSelectedCountriesWithNames() {
    return this.state.filters.selectedCountries.map(code => {
      const country = this.props.countries.find(country => country.code === code);

      return {
        code,
        name: country ? country.name : null,
      };
    });
  }

  /**
   * Event handler for adding a new country to the list of currently selected countries.
   *
   * @param countryCode
   */
  addSelectedCountry(countryCode) {
    this.updateFilters({
      selectedCountries: [ ...this.state.filters.selectedCountries, countryCode ],
    });
  }

  /**
   * Remove a given country from the list of currently selected countries.
   *
   * @param countryCode
   */
  deleteSelectedCountry(countryCode) {
    this.updateFilters({
      selectedCountries: this.state.filters.selectedCountries.filter(code => code !== countryCode),
    });
  }

  resetSelectedCountries() {
    this.updateFilters({
      selectedCountries: [],
    });
  }

  /**
   * Handle year input change event.
   *
   * @param event
   */
  handleYearChange(event) {
    this.updateFilters({
      year: parseInt(event.target.value) || 2010,
    });
  }

  /**
   * Handle population range input change event.
   *
   * @param populationRange
   */
  handlePopulationRangeChange(populationRange) {
    this.updateFilters({
      populationFrom: populationRange.from,
      populationTo: populationRange.to,
    });
  }

  /**
   * Handle country filtering mode change event.
   *
   * @param selectedMode
   */
  handleCountryFilteringModeChange(selectedMode) {
    this.updateFilters({
      countryFilteringMode: selectedMode.value,
    });
  }

  /**
   * Handle normalize checkbox toggle event.
   *
   * @param event
   */
  handleNormalizeChange(event) {
    this.updateFilters({
      normalize: event.target.checked,
    }, () => this.updateGraphs());
  }

  /**
   * Get the currently selected country filtering mode as an object containing value and label.
   *
   * @returns {T}
   */
  getCountryFilterMode() {
    return this.countryFilteringOptions.find(option => option.value === this.state.filters.countryFilteringMode);
  }


  render() {
    return (
      <>
        <div className="row">
          <div className="col-12">
            <h2>CO<sub>2</sub> emissions by country</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-lg-9">
            <div className="row">
              <div className="col-12 col-lg-4">
                <div className="form-group">
                  <label htmlFor="year">year</label>
                  <input
                    type="number" id="year" className="form-control"
                    onClick={(e) => e.target.select()}
                    value={this.state.filters.year} onChange={this.handleYearChange} />
                </div>
              </div>
              <div className="col-12 col-lg-8">
                <RangeFilter
                  options={this.populationRangeFilterOptions}
                  from={this.state.filters.populationFrom}
                  to={this.state.filters.populationTo}
                  onChange={this.handlePopulationRangeChange} />
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-lg-8">
                <CountryFilter
                  countries={this.props.countries}
                  selectedCountries={this.state.filters.selectedCountries}
                  onCountrySelected={this.addSelectedCountry} />
              </div>
              <div className="col-12 col-lg-4">
                <div className="form-group">
                  <label htmlFor="country-filtering-mode">country filtering mode</label>
                  <Select
                    inputId="country-filtering-mode"
                    value={this.getCountryFilterMode()}
                    options={this.countryFilteringOptions}
                    onChange={this.handleCountryFilteringModeChange} />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="form-group form-check">
                  <input
                    type="checkbox" id="normalize" className="form-check-input"
                    checked={this.state.filters.normalize}
                    onChange={this.handleNormalizeChange} />
                  <label htmlFor="normalize" className="form-check-label">Normalize emissions per capita</label>
                </div>
              </div>
            </div>
            <div className="row" style={{marginBottom: 40}}>
              <div className="col-12">
                <SelectedCountries
                  countries={this.getSelectedCountriesWithNames()}
                  deleteCountry={this.deleteSelectedCountry}/>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-3">
            <div className="row">
              <div className="col-12 order-2 order-lg-1">
                <div className="form-group">
                  <label className="d-none d-lg-block">&nbsp;</label>
                  <button className="btn btn-primary form-control" onClick={this.updateGraphs}>do filtering</button>
                </div>
              </div>
              <div className="col-12 order-1 order-lg-2">
                <div className="form-group">
                  <label className="d-none d-lg-block">&nbsp;</label>
                  <button className="btn btn-light form-control" onClick={this.resetSelectedCountries}>clear countries</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <EmissionsMap
              data={this.state.filteredData}
              isNormalized={this.state.filters.normalize} />
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <EmissionsByCountryGraph
              data={this.state.filteredData}
              isNormalized={this.state.filters.normalize} />
          </div>
        </div>
      </>
    );
  }
}

EmissionsByCountry.propTypes = {
  results: PropTypes.array.isRequired,
  countries: PropTypes.array.isRequired,
};

export default EmissionsByCountry;
