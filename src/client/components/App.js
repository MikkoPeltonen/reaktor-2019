import React from "react";

import "Styles/main.scss";

import API from "../api";

import EmissionsByCountry from "./EmissionsByCountry";
import EmissionsByYear from "./EmissionsByYear";


const api = new API();

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      countries: [],
      results: [],
      databaseUpdateStatus: 0,
    };

    this.fetchResults = this.fetchResults.bind(this);
    this.updateDatabase = this.updateDatabase.bind(this);
    this.getDatabaseUpdateStatusMessage = this.getDatabaseUpdateStatusMessage.bind(this);
  }

  componentDidMount() {
    // Download all data on app launch. The API supports filtering on the server side, but the results are fetched at
    // once because of the small amount of data. Filtering it on the client side using Lodash is faster than
    // making network queries each time a parameter changes.
    api.getAllCountries().then(countries => {
      this.setState({
        countries,
      });
    });

    this.fetchResults();
  }

  /**
   * Download all measurements from the API.
   */
  fetchResults() {
    api.getMesurements().then(measurements => {
      this.setState({
        results: measurements,
      });
    });
  }

  /**
   * Handle update database link click. Request server to update database.
   *
   * @param e
   */
  updateDatabase(e) {
    e.preventDefault();

    this.setState({
      databaseUpdateStatus: 1,
    });

    api.updateDatabase().then(() => {
      this.setState({
        databaseUpdateStatus: 2,
      });
    }).catch((error) => {
      this.setState({
        databaseUpdateStatus: error,
      });
    });
  }

  /**
   * Parse API database update response.
   *
   * @returns {string}
   */
  getDatabaseUpdateStatusMessage() {
    let msg = "";
    switch(this.state.databaseUpdateStatus) {
      case 0:
        break;
      case 1:
        msg = "updating database...";
        break;
      case 2:
        msg = <>database updated! <a href="/">reload page</a></>;
        break;
      default:
        msg = <>database update failed: ${this.state.databaseUpdateStatus}</>;
        break;
    }

    return msg;
  }


  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h1>CO<sub>2</sub> Emissions and Population Comparison</h1>
          </div>
        </div>

        <EmissionsByCountry countries={this.state.countries} results={this.state.results} />

        <EmissionsByYear countries={this.state.countries} results={this.state.results} />

        <div className="row update-row">
          <div className="col-12">
            {this.state.databaseUpdateStatus === 0 &&
              <p>download newest data from World Bank API: <a href="#" onClick={this.updateDatabase}>update database</a></p>
            }
            <p>{this.getDatabaseUpdateStatusMessage()}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
