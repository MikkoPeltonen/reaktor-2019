import React from "react";
import PropTypes from "prop-types";
import Select from "react-select";


class CountryFilter extends React.Component {
  constructor(props) {
    super(props);

    this.getAvailableCountryOptions = this.getAvailableCountryOptions.bind(this);
    this.handleCountrySelection = this.handleCountrySelection.bind(this);

    this.state = {
      selectValue: undefined,
    };
  }

  /**
   * Get an array of option objects for the Select component.
   *
   * @returns {{label: *, value: *}[]}
   */
  getAvailableCountryOptions() {
    // Return a list of countries that aren't present in list of currently selected countries.
    // Reformat country objects to be acceptable for React Select.
    return this.props.countries.filter(country => !this.props.selectedCountries.includes(country.code)).map(country => {
      return {
        value: country.code,
        label: country.name,
      };
    });
  }

  /**
   * Handle country selection in Select input.
   *
   * @param country
   */
  handleCountrySelection(country) {
    this.props.onCountrySelected(country.value);

    // Clear select input
    this.setState({
      selectValue: false,
    });
  }

  render() {
    return (
      <div className="row">
        <div className="col-12">
          <div className="form-group">
            <label htmlFor="country">select a country</label>
            <Select
              inputId="country"
              value={this.state.selectValue}
              options={this.getAvailableCountryOptions()}
              onChange={this.handleCountrySelection} />
          </div>
        </div>
      </div>
    );
  }
}

CountryFilter.propTypes = {
  countries: PropTypes.array.isRequired,
  selectedCountries: PropTypes.array.isRequired,
  onCountrySelected: PropTypes.func.isRequired,
};

export default CountryFilter;
