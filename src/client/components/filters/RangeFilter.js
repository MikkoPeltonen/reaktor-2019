import React from "react";
import PropTypes from "prop-types";


let idCounter = 0;

class RangeFilter extends React.Component {
  constructor(props) {
    super(props);

    this.handleFromValueChange = this.handleFromValueChange.bind(this);
    this.handleToValueChange = this.handleToValueChange.bind(this);

    // Generate unique ids for each input. idCounter will retain its value between different instances of RangeFilter.
    this.fromLabelId = `from-${idCounter++}`;
    this.toLabelId = `to-${idCounter}`;
  }

  handleFromValueChange(event) {
    let from = parseInt(event.target.value);
    if (isNaN(from)) from = this.props.from;

    this.props.onChange({
      from,
      to: this.props.to,
    });
  }

  handleToValueChange(event) {
    let to = parseInt(event.target.value);
    if (isNaN(to)) to = this.props.to;

    this.props.onChange({
      from: this.props.from,
      to,
    });
  }

  render() {
    return (
      <div className="row">
        <div className="col-12 col-sm-6">
          <div className="form-group">
            <label htmlFor={this.fromLabelId}>{this.props.options.fromLabel}</label>
            <input
              type="number" id={this.fromLabelId} className="form-control"
              onClick={(e) => e.target.select()}
              value={this.props.from} onChange={this.handleFromValueChange} />
          </div>
        </div>
        <div className="col-12 col-sm-6">
          <div className="form-group">
            <label htmlFor={this.toLabelId}>{this.props.options.toLabel}</label>
            <input
              type="number" id={this.toLabelId} className="form-control"
              onClick={(e) => e.target.select()}
              value={this.props.to} onChange={this.handleToValueChange} />
          </div>
        </div>
      </div>
    );
  }
}

RangeFilter.propTypes = {
  options: PropTypes.object.isRequired,
  from: PropTypes.number.isRequired,
  to: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default RangeFilter;
