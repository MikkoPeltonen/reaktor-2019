import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "Styles/SelectedCountries.scss";


class SelectedCountries extends React.Component {
  constructor(props) {
    super(props);

    this.getBadges = this.getBadges.bind(this);
  }

  /**
   * Render country badges as a list.
   *
   * @returns {Array}
   */
  getBadges() {
    const countryBadges = [];
    for (let country of this.props.countries) {
      countryBadges.push(
        <span key={country.code} className="countryBadge">
          {country.name}
          <FontAwesomeIcon icon="times" onClick={() => this.props.deleteCountry(country.code)} />
        </span>
      );
    }

    return countryBadges;
  }

  render() {
    return (
      <div className="row">
        <div className="col-12">
          {this.getBadges()}
        </div>
      </div>
    );
  }
}

SelectedCountries.propTypes = {
  countries: PropTypes.array.isRequired,
  deleteCountry: PropTypes.func.isRequired,
};

export default SelectedCountries;
