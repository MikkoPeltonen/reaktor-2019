const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");


const htmlWebpackPlugin = new HtmlWebpackPlugin({
  template: "./src/client/index.html",
  filename: "./index.html",
});

module.exports = {
  entry: [
    "./src/client/index.js",
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [ "@babel/preset-env", "@babel/preset-react" ],
          },
        },
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",
          },
          {
            loader: "sass-loader",
          },
        ],
      },
    ],
  },
  plugins: [
    htmlWebpackPlugin,
  ],
  devServer: {
    port: 8000,
    proxy: {
      "/api": "http://localhost:8080",
    },
  },
  resolve: {
    alias: {
      Styles: path.resolve(__dirname, "src/client/styles/"),
    },
  },
};
